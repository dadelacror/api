// importar las librerías para pruebas
var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')

// Cargo el fichero server.js con mi servidor y EJECUTA el código de ese fichero
// Es lo que hace "require"
// Es decir, levanta el servidor cuando va a hacer la prueba
var server = require('../server')

var should = chai.should()

// chaiHttp es un plugin de chai para hacer pruebas HTTP
// configurar chai con módulo HTTP
chai.use(chaiHttp)

// creamos un test suite (conjunto de pruebas)
// El segundo parámetro es una expresión Lambda, como si dijéramos un puntero a
// una funcion sin parámetros. Notacion "(param1, param2,...) =>
// Estamos pasando una función para que "describe" la ejecute por dentro
// Lo mismo que los callbacks pero con una notacion diferente

// Cada describe es una test suite o conjunto de pruebas
// Cada prueba dentro del describe se define por medio de un it

describe('Tests de conectividad', () => {
  it('Google funciona', (done) => { // "it" espera una funcion con 1 parámetro
    chai.request('http://www.google.es')
        .get ('/')
        .end ((err, res) => { //end espera una función con 2 parámetros
           //console.log (res) //Sacar la respuesta de google al GET del raíz a la consola
           res.should.have.status(200) //La respuesta HTTP debería ser 200
           done()
        })
  } )
} )


describe('Tests de API usuarios', () => {
  // Prueba de API raíz
  it('Raíz responde OK y ademas devuelve "Bienvenido a mi API"', (done) => {
    chai.request('http://localhost:3000')
        .get ('/apitechu/v1')
        .end ((err, res) => {
           res.should.have.status(200) //La respuesta HTTP debería ser 200
           // La propiedad mensaje del Json que se envia en el body debería ser
           // "Bienvenido a mi API" (ver server.js)
           res.body.mensaje.should.be.eql("Bienvenido a mi API")
           done()
        })
  } )
  //Prueba de lista de usuarios
  it('Lista de usuarios', (done) => {
    chai.request('http://localhost:3000')
        .get ('/apitechu/v1/usuarios') //Prueba del GET V1
        .end ((err, res) => {
           //La respuesta HTTP debería ser 200
           res.should.have.status(200)
           // El cuerpo de la respuesta debería ser un array
           res.body.should.be.a('array')
           // Todos los miembros del array tienen la propiedad email, first_name y last_name
           // Falla porque los regsitros de Doraemon no la tienen (lo esperado)
           for (var i = 0; i < res.body.length; i++) {
             res.body[i].should.have.property('email')
             res.body[i].should.have.property('first_name')
             res.body[i].should.have.property('last_name')
           }
           done()
        })
  } )
} )

/*
describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {

  } )
} )
*/
