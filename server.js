// importar la librería express en node.js
var express = require('express')
// importar el paquete body parser
var bodyParser = require('body-parser')
// Enable CORS
var cors = require('cors')
// Creamos el servidor (express).
// Además, usar la librería body-parser para leer JSON
var app = express()
app.use(bodyParser.json())
// Para evitar errores "No 'Access-Control-Allow-Origin' header is present on the requested resource."
app.use(cors({origin: '*'}))

//Buscamos la bibilioteca request-json
// Es un cliente HTTP que funciona mejor con json que el cliente HTTP "request"
//Definimos la URL de la API a MLAB (nuestra base de datos y nuestra ApiKey)
var requestJson = require('request-json')
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancodco/collections"
var apiKey = "apiKey=K81r6ioX1OyaEBI8p8awYt_PPWzQGVgN"

//Definimos la URL de Quandl para acceder a datos de serie histórica de acciones
var urlQuandl1 = "https://www.quandl.com/api/v3/datasets/WIKI/"
var urlQuandl2 = "/data.json"
var QuandlapiKey = "api_key=KqTJsBf8-T8hjNkWGLEx"

//Ponemos un puerto para que escuche (por defecto a variable de entorno PORT)
var port = process.env.PORT || 3000
// Importar fs -> libreria para trabajar con ficheros
var fs = require('fs')
//Escuchamos en el puerto 3000
app.listen(port)
console.log("*********************************************")
console.log("API escuchando en el puerto " + port)

/////////////////////////////////////////////////////////
//Primeros ejercicios
//Guardo en una variable el contenido del fichero "usuarios.json"
var usuarios = require('./usuarios.json')
// para el EJERCICIO 13/02
var usuarios_ejercicio = require('./usuarios_con_pw.json')
// para el ejercicio del 14/02
var cuentas = require('./cuentas.json')
/////////////////////////////////////////////////////////

console.log("*********************************************")
console.log("Tras inicializaciones, arranque de node.js OK")
console.log("*********************************************")

/////////////////////////////////////////////////////////
//
//V5 de la API de usuarios tirando ya del MongoDB de mlab
//
/////////////////////////////////////////////////////////
var clienteMlab = null;

// OPERACION login con Mlab
//
//
app.post('/apitechu/v5/usuarios/login', function (req, res) {

  var login = req.headers.login
  var password = req.headers.password

  // encontrar el documento cuyo login y password se pasa por cabecera
  var query = 'q={$and:[{"email":"' + login + '"}' + ',{"password":"'+ password +'"}]}'
  console.log(query)

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {

      if (!err){
        console.log ("Usuario devuelto por MLAB ->", body)
        if (body.length > 0){ //Login OK
          // Actualizar el objeto añadiendo el campo logado = true
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":true}}'

          // Hacemos una peticion put que actualice solo el documento cuyo ID
          // sea body[0], es decir, el usuario que ha hecho login con éxito
          // El segundo parámetro del put, post y delete es el BODY
          // Y el tercero es la funcion que se ejecutará con el PUT
          // que devolverá devolver al la api POST de arriba del todo el OK si
          // me ha hecho bien la operación $set
          clienteMlab.put('?q={"id":'+ body[0].id + '}&' + apiKey,
                          JSON.parse(cambio), //BODY del PUT
                          function(err, resP, bodyP) {
                              console.log("bodyP desde MLAB->", bodyP)
                              if (bodyP.n > 0){
                                res.send({"login":"OK",
                                           "id":body[0].id,
                                           "firstname":body[0].first_name,
                                           "lastname":body[0].last_name})
                              } else {
                                //No se encuentra en Mlab
                                res.status(404).send({"login":"Failed","id":login})
                              }
                          })
        } else {
          // Consideramos que no encuentra el usuario / password y devuelve un 404
          res.status(404).send({"login":"Failed","id":login})
        }
      }

  })
})

// LOGOUT desde MLAB
// pasándole el ID por cabecera
app.post('/apitechu/v5/usuarios/logout', function (req, res) {

  var id = req.headers.id

  // encontrar el documento cuyo login se pasa por cabecera y ademas esta logado
  var query = 'q={"id":' + id + ', "logged":true}'
  console.log(query)

  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey);
  clienteMlab.get('', function(err, resM, body) {

      if (!err){
        if (body.length > 0){ //estaba logado
          // Actualizar el objeto añadiendo el campo logado = false
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
          var cambio = '{"$set":{"logged":false}}'
          // Hacemos una peticion put que actualice solo el documento cuyo ID
          // sea body[0], es decir, el usuario que ha hecho logout
          // Ver login para detalles
          clienteMlab.put('?q={"id":'+ body[0].id + '}&' + apiKey,
                          JSON.parse(cambio), //BODY del PUT
                          function(err, resP, bodyP) {
                               res.send({"logout":"OK",
                                         "id":body[0].id,
                                         "firstname":body[0].first_name,
                                         "lastname":body[0].last_name})
                          })
        } else {
          // Consideramos que no encuentra el usuario pero devolvemos un 200
          res.status(200).send({"logout":"Not logged in previously","id":id})
        }
      }
  })
})

// Dado un id de cliente, obtener la lista de IBAN de sus cuentas
app.get('/apitechu/v5/cuentas', function (req, res) {

  var idcliente = req.headers.idcliente

  // encontrar el documento cuyo login se pasa por cabecera y ademas esta logado
  // no miramos que este logado, esto lo controla el front
  // Que devuelva solo el IBAN
  var query = 'q={"idcliente":' + idcliente + '}&f={"iban":1,"saldo":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
  //var listacuentas = []

  clienteMlab.get('', function(err, resM, body) {
      if (!err){
            //Ojo si no encuentra devuelve un array vacio. Esto habría que mejorarlo

            for (var i = 0; i < body.length; i++) {
              // Que aparezca con puntos en los miles y coma adecimal
              // Extrañamente con es-ES sale una coma en los miles y un punto en la coma decimal
              //body[i].saldo = body[i].saldo.toLocaleString('es-ES')
              body[i].saldo = parseFloat(body[i].saldo.toFixed(2)).toLocaleString('es-ES')
            }
            res.send(body)
      } else {
        // Consideramos que no encuentra el usuario pero devolvemos un 200
        res.status(404).send({"Error":"Client ID not found"})
      }
  })
})

// Dado un IBAN, obtener la lista de movimientos del IBAN
app.get('/apitechu/v5/lista_movimientos', function (req, res) {

  var iban = req.headers.iban

  // encontrar el documento cuyo login se pasa por cabecera y ademas esta logado
  // no miramos que este logado, esto lo controla el front
  var query = 'q={"iban":"' + iban + '"}&f={"iban":1,"movimientos":1,"saldo":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)

  console.log(query)

  clienteMlab.get('', function(err, resM, body) {
      if (!err){
        console.log("body[0].movimientos.length->", body[0].movimientos.length)

        for (var i = 0; i < body[0].movimientos.length; i++) {
          body[0].movimientos[i].importe = body[0].movimientos[i].importe.toFixed(2)
          //.toLocaleString('es-ES')
        }
        body[0].saldo = body[0].saldo.toFixed(2).toLocaleString('es-ES')
        res.send(body)
      } else {
        // Consideramos que no encuentra el usuario pero devolvemos un 200
        res.status(404).send({"Error":"IBAN not found"})
      }
  })
})
// Operación para crear usuario en Mlab
// Primero hacemos una peticion GET para ver si existe y si no es así
// Creamos petición POST para la db usuarios
app.post('/apitechu/v5/usuarios', function (req, res) {
  // suponemos que la peticion llega por cabeceras HTTP
  // las mostramos en consola
  // console.log(req.headers)
  // Primero revisamos que el cliente no exista, para ello hacemos una llamada get a MLAB
  var id = req.headers.id
  // filtro: solo me saca el id, quitamos el ID interno de MongoDB
  // El id me lo tiene que interpretar como un entero
  var query = 'q={"id":' + id + '}&f={"id": 1, "_id":0}'

  console.log ("1. get query", query)
  // llamamos con la query
  // Ojo que la apiKey hay que ponerla al final de la URL, para que funciona en node.js
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey);

  clienteMlab.get('', function(err, resM, body) {
      if (!err){
        // Por defecto devuelve un array de uno
        if (body.length > 0){
          console.log("2.1.Usuario ya existe: ", body[0])
          // Como el usuario ya existe devolvemos un 403 Forbidden
          res.status(403).send({result:"KO", id:req.headers.id, status:"El usuario ya existe y no se puede crear de nuevo"})

        } else {
          // Hacemos la petición POST para crear el usuario
          console.log("2.2. usuario no existe, hacer POST")
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey);
          var data =
          {
            "id" : parseInt(req.headers.id),
            "first_name" : req.headers.first_name,
            "last_name" : req.headers.last_name,
            "email" : req.headers.email,
            "gender" : req.headers.gender,
            "ip_address" : "0.0.0.0",
            "country" : req.headers.country,
            "password" : req.headers.password,
            "logged" : false
          }
          console.log("4.", data)
          // Post a MLAB, los datos del body para el POST van en el segundo parámetro
          clienteMlab.post('', data, function(err, resM, body) {
              if (!err){
                  res.send({result:"OK", id:req.headers.id, status:"new_user"})
              } else {
                  res.status(404).send({result:"KO", id:"", status:"Error en el Post a MLAB"})
              }
          })
      }
    }
  })
})

// Operación para crear movimiento en MLAB
// Hacemos un GET para traernos el documento de MLAB de tipo "cuentas" que contiene el IBAN
// pasado en la cabecera, y luego modificamos el documento y hacemos un PUT
// de la cuenta + el movimiento añadido para modificarlo
app.post('/apitechu/v5/crear_movimiento', function (req, res) {
  console.time()
  var iban = req.headers.iban
  console.log("Efectuando movimiento sobre iban: ", iban)
  // filtro: solo me saca el iban, quitamos el ID interno de MongoDB
  var query = 'q={"iban":"' + iban + '"}&f={"_id":0}'
  //console.log ("1. get query", query)
  // llamamos con la query
  // Ojo que la apiKey hay que ponerla al final de la URL, para que funciona en node.js
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey);
  var datos_cuenta = null
  clienteMlab.get('', function(err, resM, body) {
    if (!err){
      // Por defecto devuelve un array de uno
      if (body.length > 0){
        datos_cuenta = body[0]
        console.log("***cuenta y movimientos recuperados: ",datos_cuenta)
        // Hacemos un push del nuevo movimeinto
        var importe = parseFloat(req.headers.importe)
        var nuevo_mov = {
          "id":datos_cuenta.movimientos.length + 1,
          "fecha": req.headers.fecha,
          "importe": importe,
          "moneda":req.headers.moneda,
          "descr":req.headers.descr
        }
        datos_cuenta.movimientos.push(nuevo_mov)
        // añadimos el importe, si es negativo lo restamos
        datos_cuenta.saldo = datos_cuenta.saldo + importe
        console.log("***datos_cuenta tras el push",datos_cuenta)

        // Una vez construido el objeto hacemos el put de "datos_cuenta"
        clienteMlab.put('', datos_cuenta, function(err, resM, body) {
            if (!err){
                console.timeEnd()
                res.send({result:"OK", id:iban, status:"new movement OK"})
            } else {
                console.timeEnd()
                // Falla el PUT, devolvemos method not allowed
                res.status(405).send({result:"KO", id:iban, status:"PUT to Mlab failed"})
            }
        })
      } else {
        console.timeEnd()
        res.status(404).send({result:"KO", iban: iban, status:"Account not found"})
      }
    } else {
      // Error en la llamada get
      console.timeEnd()
      res.status(405).send({result:"KO", id:iban, status:"GET to Mlab failed"})
    }

  })

})

// Dado un id CLIENTE, obtener sus cuentas de valores
app.get('/apitechu/v5/cuentas_valores', function (req, res) {

  var idcliente = req.headers.idcliente

  var query = 'q={"idcliente":' + idcliente + '}&f={"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas_valores?" + query + "&" + apiKey)

  console.log(query)

  clienteMlab.get('', function(err, resM, body) {
      if (body.length != 0){
        if (!err){
          for (var i = 0; i < body[0].cuenta_valores.length; i++) {
            body[0].cuenta_valores[i].num_titulos = body[0].cuenta_valores[i].num_titulos.toLocaleString('es-ES')
            //parseFloat(body[i].saldo.toFixed(2))
          }
          res.send(body[0])
        } else {
          // Consideramos que no encuentra el usuario pero devolvemos un 200
          res.status(404).send({"Error":"ID Cliente not found"})
        }
      } else {
        // Si no tiene cuentas de valores devolvemos el array vacio como en cuentas bancarias
        res.send(body)
      }
  })
})

// Dado un ID Cliente y un ticker, obtener además precio actual y serie histórica de ese valor
// desde una fecha determinada
app.get('/apitechu/v5/cuentas_valores_detalle', function (req, res) {

  var idcliente = req.headers.idcliente
  var ticker = req.headers.ticker
  var fecha_desde = req.headers.fecha_desde
  var response

  var query = 'q={"idcliente":' + idcliente + '}&f={"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas_valores?" + query + "&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
      if (!err){
        // Iteramos por todos las cuentas de valores del cliente hasta que devolvemos
        // todos los datos más el último precio
        var found = false
        for (var i = 0; i < body[0].cuenta_valores.length && !found; i++) {
          if (body[0].cuenta_valores[i].ticker_valor === ticker){
            response = body[0].cuenta_valores[i]
            found = true
            break
          }
        }
        if (!found) {
          // Devolvemos un 404
          res.status(404).send({"Error":"Ticker not found"})
        } else {

          // Llamamos a la API de Quandl para sacar los datos del ticker
          var urlQuandl = urlQuandl1 + ticker + urlQuandl2 + "?" +
                          "column_index=4&" +
                          "start_date=" + fecha_desde + "&" +
                          "order=asc" + "&" +
                          QuandlapiKey
          //console.log("urlQuandl: " + urlQuandl)
          var clienteQuandl = requestJson.createClient(urlQuandl)
          clienteQuandl.get('', function(errQ, resQ, bodyQ) {
              if (!err){
                // Añadimos los datos de serie historica
                console.log(bodyQ.dataset_data.data)
                response["precio_cierre"] = bodyQ.dataset_data.data[bodyQ.dataset_data.data.length-1][1]
                var importe = bodyQ.dataset_data.data[bodyQ.dataset_data.data.length-1][1] * response.num_titulos
                response["importe"] = parseFloat(importe.toFixed(2)).toLocaleString('es-ES')
                response["serie_historica"] = bodyQ.dataset_data.data
                res.send(response)
              } else {
                res.status(404).send({"Error":"GET request to Quandl failed"})
              }
          })
        }

      } else {
        // Consideramos que no encuentra el usuario pero devolvemos un 200
        res.status(404).send({"Error":"ID Cliente not found"})
      }
  })
})

// GET de todos los usuarios
app.get('/apitechu/v5/usuarios', function (req, res) {

  // Creamos cliente de request-json (biblioteca para manejar peticiones json directamente sin tener que parsear)
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey);
  //console.log (clienteMlab)
  // Se llama directamente al get sin URL adicional
  // y a una funcion a que se le pasa el error, objeto response que vendrá de Mlab
  // y el body -ya en JSON- que devuelva MLAB

  clienteMlab.get('', function(err, resM, body) {
    if (!err){
      res.send(body); //Envío directamente el json
    } else {
      res.status(404).send({error:"Call to MLAB usuarios collection failed"})
    }
  })

})

// Que devuelva el nombre y apellidos de un usuario en concreto
// Se le pasa por la url, pasarlo por header duplicaría el servicio anterior
app.get('/apitechu/v5/usuarios/:id', function (req, res) {
  var id = req.params.id
  // filtro: solo me saca el nombre y apellidos del id igual al parámetro ":id"
  // Quitamos el ID interno de MongoDB
  var query = 'q={"id":' + id + '}&f={"first_name": 1, "last_name": 1, "_id":0}'
  // llamamos con la query
  // Ojo que la apiKey hay que ponerla al final de la URL, para que funciona en node.js
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey);

  clienteMlab.get('', function(err, resM, body) {
      if (!err){
        // Por defecto me devuelve un array de uno, pero no quiero devolver el array
        // por eso hacemos lo siguiente, elimina los corchetes al principio y final del JSON
        // (devuelve solo el elemento no el array)
        if (body.length > 0){
          res.send(body[0]); //Envío directamente el json
        } else {
          // Consideramos que no encuentra el recurso y devuelve un 404
          res.status(404).send({error:"User not found: " + id})
      }
    }
  })
})



////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// A PARTIR DE AQUI TENEMOS LOS PRIMEROS EJERCICIOS HECHOS EN CLASE

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

// Petición GET sobre /apitechu/V1, nos deveulve la doc de la API
app.get('/apitechu/v1', function (req, res) {
  //Sacamos la request en consola (se ve en el terminal donde corre node)
  // console.log(req)
  // Devolvemos un JSON en la respuesta
  // OJO, un objeto JSON: no lo estamos encerrando en comillas simples!!
  res.send({"mensaje":"Bienvenido a mi API"})
})

// Devolucion de la lista de usuarios generada en mockaroo

app.get('/apitechu/v1/usuarios', function (req, res) {
  // Devolvemos con el objeto res el fichero usuarios.json
  //res.sendfile("usuarios.json")
  // Tras hacer el require('usuarios.json')
  //console.log(usuarios)
  res.send(usuarios)
})

// Creamos petición POST para usuarios
app.post('/apitechu/v1/usuarios', function (req, res) {
  // suponemos que la peticion llega por cabeceras HTTP
  // las mostramos en consola
  console.log(req.headers)
  var nuevo = {"first_name":req.headers.first_name,
              "country":req.headers.country}
  // la variable usuarios es un array, hacemos un push
  usuarios.push(nuevo)
  // Guardamos los nuevos datos en el fichero
  // Convertimos el objeto json en cadenas de caracteres
  const datos = JSON.stringify(usuarios)
  //Y lo Guardamos
  // err es una funcion callback que se ejecuta siempre, es el ultimo parametro
  // de fs.writefile
  fs.writeFile("./usuarios.json", datos, "utf8", function err(){
    if (err) {
      console.log(err)
    } else {
      console.log("Fichero guardado")
    }
  })
  // Siempre un send para que el cliente no se quede esperando eternamente
  res.send("Alta OK")
})

// Creamos petición DELETE
// Eliminando un usaurio en concreto, hay que pasarlo de alguna manera
// querystring, cabeceras HTTP, o como quiera
// Con este parámetro en la URI NODE.JS identifica que :id es un parámetro
app.delete('/apitechu/v1/usuarios/:id', function(req,res) {
  // Borra el array de usuarios: pasamos indice y cuantos elementos a borrar
  // Con req.params.id estamos obteniendo el parámetro id
  usuarios.splice(req.params.id - 1, 1)
  res.send("Usuario borrado")
})

//Petición POST usando BODY con dos parámetros
// Para ver que valores tienen los sub-objetos de req
app.post('/apitechu/v1/monstruo/:p1/:p2', function (req,res){
  console.log("Parámetros --->")
  console.log("========================================")
  console.log(req.params)
  console.log("========================================")
  console.log("Query strings --->")
  console.log("========================================")
  console.log(req.query)
  console.log("========================================")
  console.log("Headers --->")
  console.log("========================================")
  console.log(req.headers)
  console.log("========================================")
  // Ojo que el body es "undefined" hasta que no instalemos bodyParser
  // Ver arriba
  console.log("Body --->")
  console.log("========================================")
  console.log(req.body)
  console.log("========================================")
  res.send("Peticion 'MONSTRUO' finalizada OK")

})

// Peticion POST con bodies
// Ponemos de paso una version 2 para la API usuarios
app.post('/apitechu/v2/usuarios', function (req, res) {
  // el body ya viene en JSON
  var nuevo = req.body
  // la variable usuarios es un array, hacemos un push
  usuarios.push(nuevo)
  // Guardamos los nuevos datos en el fichero
  // Convertimos el objeto json en cadenas de caracteres
  const datos = JSON.stringify(usuarios)
  //Y lo Guardamos
  // err es una funcion callback que se ejecuta siempre, es el ultimo parametro
  // de fs.writefile
  fs.writeFile("./usuarios.json", datos, "utf8", function err(){
    if (err) {
      console.log(err)
    } else {
      console.log("Fichero guardado")
    }
  })
  // Siempre un send para que el cliente no se quede esperando eternamente
  res.send("Alta via JSON OK")
})

// EJERCICIO 1!!!
// OPERACION login
// Peticion POST con bodies
app.post('/apitechu/v2/usuarios/login', function (req, res) {

  var login = req.headers.login
  var password = req.headers.password
  var encontrado = false
  var idusuario = 0

  // la variable usuarios_ejercicio es un array
  // Buscamos en el array si existe el login que llega en la cabecera HTTP
  for (var i in usuarios_ejercicio) {

    if (usuarios_ejercicio[i].email == login &&
        usuarios_ejercicio[i].password == password)
        {
          encontrado = true
          idusuario = usuarios_ejercicio[i].id
          //Le añadimos una propiedad al json sin problema
          usuarios_ejercicio[i].logged = true
          console.log(usuarios_ejercicio[i])
          break
        }
  }

  // Guardamos a un fichero para ver el resultado del nuevo campo "logged"

    /*
    const datos2 = JSON.stringify(usuarios_ejercicio)
    console.log(datos2)
    fs.writeFile("./usuarios_resultado_ejercicio.json", datos2, "utf8", function err(){
    if (err) {
      console.log(err)
    } else {
      console.log("Fichero guardado")
    }
    */

  // devolvemos JSON con el resultado del login
  if (encontrado){
    res.send({"login_status":"ok","idusuario":idusuario,"login_message":"Login correcto, bienvenido"})
  } else{
    res.send({"login_status":"login_failed", "idusuario":idusuario,"login_message":"Usuario o password incorrecto"})
  }
})

// EJERCICIO 2!!!
// OPERACION logout
// Peticion POST con bodies
app.post('/apitechu/v2/usuarios/logout', function (req, res) {

  var login = req.headers.login
  var logout = false
  var idusuario = 0

  // la variable usuarios_ejercicio es un array
  // Buscamos en el array si existe el login que llega en la cabecera HTTP
  for (var i in usuarios_ejercicio) {
    //console.log(usuarios_ejercicio[i].email)
    //console.log(usuarios_ejercicio[i].logged)

    if (usuarios_ejercicio[i].email == login &&
        usuarios_ejercicio[i].logged == true)
    {
      logout = true
      idusuario = usuarios_ejercicio[i].id
      //Le cambiamos la propiedad logged a false
      usuarios_ejercicio[i].logged = false
      console.log(usuarios_ejercicio[i])
      break
    }
  }
  // devolvemos JSON con el resultado del login
  if (logout){
    res.send({"login_status":"logged_out_ok","idusuario":idusuario,"login_message":"Logout correcto, gracias!"})
  } else{
    res.send({"login_status":"logout_failed", "idusuario":idusuario,"login_message":"Usuario incorrecto, falló el logout"})
  }
})

// EJERCICIO 14/02
// 1) Devolucion de las cuentas generadas en mockaroo (los iban)
app.get('/apitechu/v1/lista_cuentas', function (req, res) {

  //res.send(cuentas)
  var lista_cuentas = []

  for (var i = 0; i < cuentas.length; i++) {
    lista_cuentas.push({"idcliente":cuentas[i].idcliente,"iban":cuentas[i].iban})
  }
  res.send(lista_cuentas)
})

// 2) Devolucion de todos los movimientos de una cuenta
app.get('/apitechu/v1/movimientos_cuenta', function (req, res) {

  //La cuenta viene en la cabecera HTTP
  var cuenta = req.headers.cuenta
  var encontrado = false

  //Busco la cuenta en el array
  for (var i in cuentas) {

    if (cuentas[i].iban == cuenta) {
      encontrado = true
      // Suponemos que las cuentas no se repiten en el fichero
      break
      }
  }

  if (encontrado){
    res.send(cuentas[i].movimientos)
  } else{
    res.send({"error":"no_accounts_found"})
  }
})

// 3) Dado un cliente que devuelva las cuentas
app.get('/apitechu/v1/cuentas_cliente', function (req, res) {

  //El cliente viene en la cabecera HTTP
  var cliente = req.headers.idcliente
  var encontrado = false
  var respuesta = []

  //Busco la cuenta en el array y le hago un push a la respuesta
  for (var i in cuentas) {

    if (cuentas[i].idcliente == cliente) {
      respuesta.push({"iban":cuentas[i].iban})
      }
  }

  if (respuesta.length == 0)
    respuesta = "no_accounts_found"

  res.send({"idcliente":cliente,respuesta})

})
